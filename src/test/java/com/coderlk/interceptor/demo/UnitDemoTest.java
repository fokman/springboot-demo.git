package com.coderlk.interceptor.demo;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class UnitDemoTest {

    @Test
    void basicAssertions() {
        assertEquals(2 + 2, 4, "简单的加法");
        assertTrue("foo".startsWith("f"), "字符串开始于 'f'");
        assertNull(null, "值应该是null");
    }

    @Test
    void collectionAssertions() {
        String[] expected = {"a", "b", "c"};
        String[] actual = {"a", "b", "c"};
        assertArrayEquals(expected, actual, "数组应该相等");

        List<String> expectedList = Arrays.asList("apple", "banana", "orange");
        List<String> actualList = Arrays.asList("apple", "banana", "orange");
        assertIterableEquals(expectedList, actualList, "列表应该相等");
    }

    @Test
    void exceptionAssertions() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            throw new IllegalArgumentException("一个非法参数异常");
        });
        assertEquals("一个非法参数异常", exception.getMessage());
    }

    @Test
    void multipleAssertions() {
        assertAll(
                () -> assertEquals(2, 2),
                () -> assertTrue("hello".startsWith("he")),
                () -> assertFalse("test".isEmpty())
        );
    }
}
