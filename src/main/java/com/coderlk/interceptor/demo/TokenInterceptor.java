package com.coderlk.interceptor.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import java.util.LinkedHashMap;
import java.util.Map;

public class TokenInterceptor implements HandlerInterceptor {
    @Override
    /*
    * preHandle方法内部代码主要执行以下操作：
    * 1. 首先检查HTTP请求头中是否包含有效的token
    * 2. 如果token存在，则通过用户身份校验，请求可以继续处理；
    * 3. 如果token不存在，方法会构建一个错误信息响应，以JSON格式返回给客户端，并终止请求继续向下执行。
    * */
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        if(token != null) {
            return true;
        }else{
            ObjectMapper objectMapper = new ObjectMapper();
            Map<String,String> result = new LinkedHashMap<>();
            result.put("code", "INVALID_TOKEN");
            result.put("message", "请求未包含Token,用户校验失败");
            response.setContentType("application/json;charset=utf-8");
            response.setStatus(401);
            response.getWriter().println(objectMapper.writeValueAsString(result));
            return false;
        }

    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}