package com.coderlk.interceptor.demo.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AppConfiguration {
    
    @Value("${spring.application.name}")
    public String appName;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    @ConfigurationProperties(prefix = "app.config")
    public AppConfigProperties appConfigProperties() {
        return new AppConfigProperties();
    }

    @Bean
    public AppConfigProperties appConfigPropertiesNon() {
        return new AppConfigProperties();
    }

    @Bean
    public UploadTools uploadTools(@Qualifier("appConfigProperties") AppConfigProperties properties) {
        UploadTools uploadTools = new UploadTools();
        uploadTools.setAppConfigProperties(properties);
        return uploadTools;
    }
}
