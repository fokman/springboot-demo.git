package com.coderlk.interceptor.demo.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class UploadTools {
    private AppConfigProperties appConfigProperties;

    public void upload() {
        log.info("app.config:{}", appConfigProperties);
    }
}
