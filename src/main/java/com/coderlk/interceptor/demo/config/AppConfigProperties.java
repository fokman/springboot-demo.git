package com.coderlk.interceptor.demo.config;

import lombok.Data;

@Data
public class AppConfigProperties {
    private String uploadAddr;
    private String appKey;
}
