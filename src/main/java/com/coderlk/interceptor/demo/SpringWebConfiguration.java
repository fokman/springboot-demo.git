package com.coderlk.interceptor.demo;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class SpringWebConfiguration implements WebMvcConfigurer {

    /**
     * 注册拦截器并配置其拦截路径。
     *
     * @param registry 拦截器注册表，用于添加和管理拦截器。
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 创建一个TokenInterceptor实例并注册为拦截器
        InterceptorRegistration registration = registry.addInterceptor(new TokenInterceptor());


        // 配置该拦截器拦截所有路径
        registration.addPathPatterns("/**");

        // 配置该拦截器不拦截的路径，如静态资源路径
        registration.excludePathPatterns("/img/**", "/static/**");
    }

    /**
     * 添加资源处理器，用于处理静态资源请求。
     *
     * @param registry 资源处理器注册表，用于添加和管理资源处理器。
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 添加资源处理器，将"/static/**"路径的请求映射到"classpath:/static/"目录下的资源
        // 1.jpg -> url: http://localhost:8080/static/1.jpg
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
    }
}