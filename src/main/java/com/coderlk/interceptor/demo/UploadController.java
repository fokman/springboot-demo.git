package com.coderlk.interceptor.demo;

import com.coderlk.interceptor.demo.config.UploadTools;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UploadController {
    @Resource
    UploadTools uploadTools;

    @GetMapping("/uploader")
    public void upload() {
        uploadTools.upload();
    }

}